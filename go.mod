module gitlab.com/slhamptonii/sandbox-items

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.9.0
	github.com/stretchr/testify v1.7.0
	gitlab.com/slhamptonii/sandbox-users v0.0.0-20210121020127-e27cf5e84354
	gitlab.com/slhamptonii/sheldonsandbox-core/v3 v3.5.2
	gopkg.in/yaml.v2 v2.4.0
)
