package crypt

import (
	"golang.org/x/crypto/bcrypt"
	"log"
)

func ComparePasswords(hashed string, plain []byte) bool {
	// Since we'll be getting the hashed password from the DB it
	// will be a string so we'll need to convert it to a byte slice
	byteHash := []byte(hashed)
	err := bcrypt.CompareHashAndPassword(byteHash, plain)
	if err != nil {
		log.Println("could not verify password: ", err)
		return false
	}

	return true
}

// Hash and Salt value with the bcrypt MaxCost (31).
// This function is extremely expensive and only to
// be used as a necessity.
func HashAndSaltMax(val []byte) string {
	return hashAndSalt(val, bcrypt.MaxCost)
}

// Hash and Salt value with the bcrypt DefaultCost (10).
func HashAndSaltDefault(val []byte) string {
	return hashAndSalt(val, bcrypt.DefaultCost)
}

// Hash and Salt value with the bcrypt MinCost (4).
func HashAndSaltMin(val []byte) string {
	return hashAndSalt(val, bcrypt.MinCost)
}

func hashAndSalt(val []byte, cost int) string {
	// Use GenerateFromPassword to hash & salt val
	// MinCost is just an integer constant provided by the bcrypt
	// package along with DefaultCost & MaxCost.
	// The cost can be any value you want provided it isn't lower
	// than the MinCost (4)
	hash, err := bcrypt.GenerateFromPassword(val, cost)
	if err != nil {
		log.Println("could not hash and salt password: ", err)
		return ""
	}
	return string(hash)
}
