package model

type Item struct {
	Id          int    `json:"id,omitempty" validate:"min=0"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Owner       int `json:"owner,omitempty" validate:"min=0"`
	CreatedDateTime string   `json:"createdDateTime"`
	UpdatedDateTime string   `json:"updatedDateTime"`
}
