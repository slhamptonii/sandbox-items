package model

type Set struct {
	Weight float32 `json:"weight,omitempty,string" `
	Reps   int8    `json:"reps,omitempty,string" `
}
