package model

type Party struct {
	Id       int      `json:"id,omitempty" validate:"min=0"`
	Name     string   `json:"name"`
	DM       string   `json:"dm"`
	Members  []int `json:"members,omitempty" validate:"min=0"`
	Password string   `json:"password"`
	CreatedDateTime string   `json:"createdDateTime"`
	UpdatedDateTime string   `json:"updatedDateTime"`
}
