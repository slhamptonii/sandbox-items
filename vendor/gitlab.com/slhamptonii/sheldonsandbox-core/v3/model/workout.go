package model

type Workout struct {
	Id              int       `json:"id,omitempty" validate:"min=0"`
	Name            string    `json:"name,omitempty" `
	UserId          int       `json:"userId,omitempty" `
	CreatedDateTime string    `json:"createdDateTime,omitempty" `
	UpdatedDateTime string    `json:"updatedDateTime,omitempty" `
	Exercises       Exercises `db:"exercises" json:"exercises,omitempty" `
}
