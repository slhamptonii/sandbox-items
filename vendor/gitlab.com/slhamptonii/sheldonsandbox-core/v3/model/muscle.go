package model

type MuscleGroup string

var MuscleGroups = []MuscleGroup{Arms, Back, Chest, Core, Heart, Legs, Shoulders}

type Muscle struct {
	Name        string      `db:"name" json:"name,omitempty" `
	MuscleGroup MuscleGroup `db:"muscleGroup" json:"muscleGroup,omitempty" `
}

const (
	Arms      MuscleGroup = "Arms"
	Back      MuscleGroup = "Back"
	Chest     MuscleGroup = "Chest"
	Core      MuscleGroup = "Core"
	Heart     MuscleGroup = "Heart"
	Legs      MuscleGroup = "Legs"
	Shoulders MuscleGroup = "Shoulders"
)
