package controller

import (
	"errors"
	"gitlab.com/slhamptonii/sandbox-users/service"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v3/crypt"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v3/model"
	"log"
	"net/http"
)

type PasswordChange struct {
	Id               int    `json:"id"`
	OriginalPassword string `json:"originalPassword"`
	NewPassword      string `json:"newPassword"`
}

//endpoint to reset password
func ResetPassword(w http.ResponseWriter, r *http.Request) {
	passwordChange, err := payloadToPasswordChange(r)
	defer r.Body.Close()
	if err != nil {
		handleUserErr(w, err)
		return
	}

	originalUser, err := service.GetByID(passwordChange.Id)
	if err != nil {
		log.Println("could not get user", err.Error())
		handleUserErr(w, err)
		return
	}


	if !crypt.ComparePasswords(originalUser.Password, []byte(passwordChange.OriginalPassword)) {
		log.Println("bad password reset")
		handleUserErr(w, errors.New(invalidUsernamePassword))
		return
	}

	newPassword := crypt.HashAndSaltDefault([]byte(passwordChange.NewPassword))
	if newPassword == "" {
		handleUserErr(w, errors.New("unable to protect password"))
		return
	}

	user := model.User{Id: passwordChange.Id, Password: newPassword}

	err = service.UpdateUser(&user, true)
	if err != nil {
		handleUserErr(w, err)
		return
	}

	user, err = service.GetByID(user.Id)
	if err != nil {
		log.Println("could not get user", err.Error())
		handleUserErr(w, err)
		return
	}

	//successfully found user
	//remove password from response
	user.Password = ""

	respondWithJSON(w, http.StatusOK, user)
}


//endpoint to "login" to application via email
func Login(w http.ResponseWriter, r *http.Request) {
	stranger, err := payloadToUser(r)
	defer r.Body.Close()
	if err != nil {
		handleUserErr(w, err)
		return
	}

	user, err := service.GetByUsername(stranger.Username)

	if err != nil {
		log.Println("error finding user", err.Error())
		handleUserErr(w, err)
		return
	}

	if user.Password != "" {
		if !crypt.ComparePasswords(user.Password, []byte(stranger.Password)) {
			handleUserErr(w, errors.New(invalidUsernamePassword))
			return
		}
	} else {
		handleUserErr(w, errors.New("not found"))
		return
	}

	user.Password = ""
	respondWithJSON(w, http.StatusOK, user)
}

//endpoint to "logout" user
func Logout(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("x-ss", "")
	respondWithJSON(w, http.StatusNoContent, nil)
}