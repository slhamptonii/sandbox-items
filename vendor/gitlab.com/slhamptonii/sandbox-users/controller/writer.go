package controller

import (
	"encoding/json"
	"net/http"
)

//Clears signed string token and sets HTTP Response body to map of error messages
func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

//Sets status, body, and default headers to HTTP Response
func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	_, _ = w.Write(response)
}
