package controller

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/slhamptonii/sandbox-users/service"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v3/model"
)

const invalidId = "invalid user"
const invalidUsernamePassword = "invalid username password combination"

//endpoint to create a new user
func CreateUser(w http.ResponseWriter, r *http.Request) {
	user, err := payloadToUser(r)
	if err != nil {
		handleUserErr(w, err)
		return
	}

	err = r.Body.Close()
	if err != nil {
		handleUserErr(w, err)
		return
	}

	err = service.CreateUser(&user)
	if err != nil {
		handleUserErr(w, err)
		return
	}

	//remove password from response
	user.Password = ""

	respondWithJSON(w, http.StatusCreated, user)
}

//endpoint to retrieve a single user by their id
func GetUser(w http.ResponseWriter, r *http.Request) {
	id, err := extractIdFromUrl(r)
	if err != nil {
		handleUserErr(w, err)
		return
	}

	user, err := service.GetByID(id)
	if err != nil {
		log.Println("could not get user", id, err.Error())
		handleUserErr(w, err)
		return
	}

	//successfully found user
	//remove password from response
	user.Password = ""

	//data layer will always return a model.User type
	//valid user id should always be greater than 0
	if user.Id >= 0 {
		respondWithJSON(w, http.StatusOK, user)
	} else {
		respondWithError(w, http.StatusNotFound, "User not found")
	}
}

func extractIdFromUrl(r *http.Request) (int, error) {
	vars := mux.Vars(r)
	userId := vars["userId"]

	//convert userId parameter to an int
	id, err := strconv.Atoi(userId)
	if err != nil {
		log.Println("could not convert id to int")
		return -1, err
	}
	return id, nil
}

//endpoint to retrieve multiple users
func GetUsers(w http.ResponseWriter, r *http.Request) {
	//default one user
	//TODO: set in config
	userCount := 1
	c := r.URL.Query().Get("count")

	if c != "" {
		count, err := strconv.Atoi(c)
		if count < 1 {
			count = 1
		}
		userCount = count
		if err != nil {
			handleUserErr(w, err)
			return
		}
	}

	users, err := service.GetUsers(userCount)
	if err != nil {
		handleUserErr(w, err)
		return
	}

	respondWithJSON(w, http.StatusOK, users)
}

//endpoint to update a user
func UpdateUser(w http.ResponseWriter, r *http.Request) {
	user, err := payloadToUser(r)
	defer r.Body.Close()
	if err != nil {
		handleUserErr(w, err)
		return
	}

	id, err := extractIdFromUrl(r)
	if err != nil {
		handleUserErr(w, err)
		return
	}

	if user.Id != id {
		log.Println("invalid ids for update", user.Id, id)
		handleUserErr(w, errors.New(invalidId))
		return
	}

	err = service.UpdateUser(&user, false)
	if err != nil {
		handleUserErr(w, err)
		return
	}

	user, err = service.GetByID(id)
	if err != nil {
		log.Println("could not get user", id, err.Error())
		handleUserErr(w, err)
		return
	}

	//successfully found user
	//remove password from response
	user.Password = ""

	respondWithJSON(w, http.StatusOK, user)
}

//endpoint to delete user
func DeleteUser(w http.ResponseWriter, r *http.Request) {
	id, err := extractIdFromUrl(r)
	if err != nil {
		handleUserErr(w, err)
		return
	}

	if id < 0 {
		handleUserErr(w, errors.New("not found"))
		return
	}

	err = service.DeleteUser(id)
	if err != nil {
		handleUserErr(w, err)
		return
	}

	respondWithJSON(w, http.StatusNoContent, nil)
}

//map service errors to valid HTTP Response codes
func handleUserErr(w http.ResponseWriter, err error) {
	var e = err.Error()
	var status = 0

	if e == "not found" || e == "sql: no rows in result set" {
		status = http.StatusNotFound
		e = "User not found"
	} else if strings.Contains(e, "invalid ObjectId in JSON:") {
		status = http.StatusBadRequest
		e = "invalid ObjectId in JSON"
	} else if strings.Contains(e, "Error:Field validation") {
		status = http.StatusBadRequest
	} else if strings.Contains(e, "validationErrs:") {
		status = http.StatusBadRequest
	} else if e == invalidUsernamePassword || e == invalidId {
		status = http.StatusForbidden
	} else if strings.Contains(e, "u_username") {
		status = http.StatusBadRequest
		e = "Invalid name. User already exists"
	} else {
		log.Println("unknown error", e)
		status = http.StatusInternalServerError
		e = "internal service error"
	}
	respondWithError(w, status, e)
}

//Converts HTTP request body into a model.User struct
func payloadToUser(r *http.Request) (model.User, error) {
	var user model.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		log.Println("error decoding user", err.Error())
	}
	return user, err
}

//Converts HTTP request body into a model.User struct
func payloadToPasswordChange(r *http.Request) (PasswordChange, error) {
	var passwordChange PasswordChange
	err := json.NewDecoder(r.Body).Decode(&passwordChange)
	if err != nil {
		log.Println("error decoding user", err.Error())
	}
	return passwordChange, err
}
