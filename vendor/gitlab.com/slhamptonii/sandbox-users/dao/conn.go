package dao

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
)

type Dao struct {
	DB *sql.DB
}

var db Dao

func Initialize(host, port, username, password, dbname, ssl string) bool {
	var err error

	//connect to database
	connectionString := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		host, port, username, password, dbname, ssl)

	db.DB, err = sql.Open("postgres", connectionString)
	if err != nil {
		log.Fatal(err)
		return false
	}

	//test connection
	if err := db.DB.Ping(); err != nil {
		log.Fatal("could not ping Db", err.Error())
		return false
	}

	log.Println("successfully connected to db")
	return true
}

func getDB() *sql.DB {
	return db.DB
}
