package dao

import (
	"errors"
	"fmt"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v3/model"
	"log"
)

func CreateUser(user *model.User) error {
	roleIds, err := checkRoles(user.Roles)
	if err != nil {
		return err
	}

	err = getDB().QueryRow(
		"INSERT INTO sandbox.user(username, password,  created_date_time, updated_date_time, meta) VALUES($1, $2, $3, $4, $5) RETURNING id",
		user.Username, user.Password, user.CreatedDateTime, user.UpdatedDateTime, user.Meta).Scan(&user.Id)

	if err != nil {
		log.Println("could not create user", err.Error())
	}

	for _, roleId := range roleIds {
		getDB().QueryRow("INSERT INTO sandbox.user_roles(user_id, role_id) VALUES ($1, $2)", user.Id, roleId)
	}

	return err
}

func checkRoles(roles []model.Role) ([]int, error) {
	rows, err := getDB().Query("SELECT * FROM sandbox.role")
	if err != nil {
		log.Println("error verifying roles", err.Error())
		return []int{}, err
	}

	roleIdMap := map[int]model.Role{}
	userRoles := make([]int, 0)
	for rows.Next() {
		var role model.Role
		var id int
		if err := rows.Scan(&id, &role); err != nil {
			log.Println("error verifying roles", err.Error())
			return []int{}, err
		}
		roleIdMap[id] = role
	}

	for _, role := range roles {
		ok := false
		for id, roleName := range roleIdMap {
			if role == roleName {
				ok = true
				userRoles = append(userRoles, id)
				break
			}
		}

		if !ok {
			return []int{}, errors.New(fmt.Sprintf("invalid role %v", role))
		}
	}


	return userRoles, nil
}

func GetByID(id int) (model.User, error) {
	user := model.User{}
	return user, getDB().QueryRow("SELECT * FROM sandbox.user WHERE id=$1", id).Scan(
		&user.Id, &user.Username, &user.Password, &user.CreatedDateTime, &user.UpdatedDateTime, &user.Meta)
}

func GetByUsername(username string) (model.User, error) {
	user := model.User{}
	return user, getDB().QueryRow("SELECT * FROM sandbox.user WHERE username=$1", username).Scan(
		&user.Id, &user.Username, &user.Password, &user.CreatedDateTime, &user.UpdatedDateTime, &user.Meta)
}

func GetUserRoles(id int) ([]model.Role, error) {
	roles := []model.Role{}
	rows, err := getDB().Query("SELECT role.ROLE FROM sandbox.role WHERE role.id IN (SELECT user_roles.role_id FROM sandbox.user_roles WHERE user_id=$1)", id)
	if err != nil {
		log.Println("unable to get user roles for user", id, err.Error())
		return nil, err
	}

	for rows.Next() {
		var role model.Role
		if err := rows.Scan(&role); err != nil {
			log.Println("unable to get user roles for user", id, err.Error())
			return nil, err
		}
		roles = append(roles, role)
	}

	return roles, nil
}

func GetUsers(count int) ([]model.User, error) {
	output := make([]model.User, 0)
	rows, err := getDB().Query(
		"SELECT * FROM sandbox.user LIMIT $1", count)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var u model.User
		if err := rows.Scan(&u.Id, &u.Username, &u.Password, &u.CreatedDateTime, &u.UpdatedDateTime, &u.Meta); err != nil {
			return nil, err
		}
		output = append(output, u)
	}
	return output, nil
}

//func GetByEmail(email string) (model.User, error) {
//	user := model.User{}
//	return user, getDB().QueryRow("SELECT * FROM bar.user WHERE email = $1", email).Scan(
//		&user.Id, &user.Name, &user.Password, &user.Email, &user.CreatedDateTime, &user.UpdatedDateTime,
//		&user.Age, &user.Height, &user.Weight, &user.Role, &user.Rank)
//}

func UpdateUser(user *model.User, resetPassword bool) error { //UPDATE sandbox.user SET age = 25 WHERE id = 99;
	var err error
	if resetPassword {
		_, err = getDB().Exec("UPDATE sandbox.user SET password = $1, updated_date_time = $2 WHERE id = $3",
			user.Password, user.UpdatedDateTime, user.Id)
	} else {
		_, err = getDB().Exec("UPDATE sandbox.user SET username = $1, updated_date_time = $2 WHERE id = $3",
			user.Username, user.UpdatedDateTime, user.Id)
	}
	return err
}

func DeleteUser(id int) error {
	_, err := getDB().Exec("DELETE FROM sandbox.user_roles WHERE user_id=$1", id)
	if err != nil {
		log.Println("could not delete user", id, err.Error())
		return err
	}
	_, err = getDB().Exec("DELETE FROM sandbox.user WHERE id=$1", id)
	if err != nil {
		log.Println("could not delete user", id, err.Error())
		return err
	}

	return nil
}
