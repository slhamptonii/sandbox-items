package service

import (
	"time"
)

const format = time.UnixDate

func nowUTC() string {
	return time.Now().UTC().Format(format)
}

func toTimeUTC(s string) (time.Time, error) {
	return time.Parse(format, s)
}

func toStringUTC(t time.Time) string {
	return t.Format(format)
}
