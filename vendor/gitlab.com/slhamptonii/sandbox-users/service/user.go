package service

import (
	"errors"
	"gitlab.com/slhamptonii/sandbox-users/dao"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v3/crypt"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v3/model"
	"log"
)

const (
	countMax = 100 // maximum number of users to pull from db
	countMin = 1   // minimum number of users to pull from db
)

//validates and updates user meta pre and post persistence
func CreateUser(user *model.User) error {
	user.Password = crypt.HashAndSaltDefault([]byte(user.Password))
	if user.Password == "" {
		return errors.New("unable to protect password")
	}

	updateForCreate(user)
	err := dao.CreateUser(user)
	if err != nil {
		return err
	}

	//successfully created user
	//stripping password from response
	user.Password = ""
	return nil
}

func GetByID(id int) (model.User, error) {
	log.Println("ready to get user", id)
	user, err := dao.GetByID(id)
	if err != nil {
		return model.User{}, err
	}

	roles, err := dao.GetUserRoles(user.Id)
	if err != nil {
		return model.User{}, err
	}

	user.Roles = roles

	return user, nil
}

func GetByUsername(username string) (model.User, error) {
	user, err := dao.GetByUsername(username)
	if err != nil {
		return model.User{}, err
	}

	roles, err := dao.GetUserRoles(user.Id)
	if err != nil {
		return model.User{}, err
	}

	user.Roles = roles

	return user, nil
}
//
//func GetByEmail(ctx context.Context, email string) (model.User, error) {
//	l.Info(ctx, "ready to get user: %v", email)
//	return dao.GetByEmail(email)
//}
//
func GetUsers(count int) ([]model.User, error) {
	count = checkCount(count)

	return dao.GetUsers(count)
}

func UpdateUser(user *model.User, resetPassword bool) error {
	user.UpdatedDateTime = nowUTC()
	log.Println("ready to update user", user.Id)
	return dao.UpdateUser(user, resetPassword)
}

func DeleteUser(id int) error {
	log.Println("ready to delete user", id)
	return dao.DeleteUser(id)
}

//populate timestamp fields for new instance
//update role to entry role
func updateForCreate(user *model.User) {
	user.CreatedDateTime = nowUTC()
	user.UpdatedDateTime = nowUTC()
	user.Roles = []model.Role{model.PLAYER}
}

//enforces count limits
func checkCount(count int) int {
	if count > countMax {
		return countMax
	}

	if count < countMin {
		return countMin
	}

	return count
}
