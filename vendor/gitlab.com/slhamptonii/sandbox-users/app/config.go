package app

import (
	"fmt"
	"io/ioutil"
	"log"
	"path/filepath"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Dao struct {
		Ssl      string `yaml:"ssl"`
		Host     string `yaml:"host"`
		Password string `yaml:"password"`
		Db       string `yaml:"db"`
		Port     string `yaml:"port"`
		Username string `yaml:"username"`
	} `yaml:"dao,omitempty"`
}

func load(env string) (Config, bool) {
	//load config file
	var config Config
	path := fmt.Sprintf("./env/%s.yml", env)
	envPath, _ := filepath.Abs(path)

	data, err := ioutil.ReadFile(envPath)
	if err != nil {
		log.Fatalf("could not read env config file %v", err)
		return config, false
	}

	//unmarshal config
	err = yaml.Unmarshal(data, &config)
	if err != nil {
		log.Fatalf("could not unmarshall yaml file %v", err)
		return config, false
	}

	return config, true
}
