package app

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/slhamptonii/sandbox-users/controller"
	"gitlab.com/slhamptonii/sandbox-users/dao"
)

type App struct {
	Router *mux.Router
	Config Config
}

func (a *App) Initialize(env string) bool {
	//load a.Config
	config, ok := load(env)
	if !ok {
		return false
	}
	a.Config = config

	//TODO: make into fun([]func(Config,string)(bool)
	ok = dao.Initialize(
		a.Config.Dao.Host, a.Config.Dao.Port, a.Config.Dao.Username,
		a.Config.Dao.Password, a.Config.Dao.Db, a.Config.Dao.Ssl)
	if !ok {
		return false
	}

	a.Router = mux.NewRouter()
	a.initializeRoutes()

	return true
}

func (a *App) Run(portHttp string) {
	// Start the HTTP server and redirect all incoming connections to HTTPS
	err := http.ListenAndServe(":"+portHttp, a.Router)

	if err != nil {
		log.Fatal(err)
	}
}

func (a *App) initializeRoutes() {
	//a.Router.Use(l.Logging)
	a.Router.Methods("HEAD").Path("/health").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println("healthy")
		w.WriteHeader(http.StatusOK)
	})

	a.Router.Methods("POST").Path("/login").HandlerFunc(controller.Login)
	a.Router.Methods("POST").Path("/logout").HandlerFunc(controller.Logout)

	a.Router.Methods("POST").Path("/users").HandlerFunc(controller.CreateUser)
	a.Router.Methods("GET").Path("/users").HandlerFunc(controller.GetUsers)
	a.Router.Methods("GET").Path("/users/{userId:[0-9]+}").HandlerFunc(controller.GetUser)
	a.Router.Methods("PATCH").Path("/users/{userId:[0-9]+}").HandlerFunc(controller.UpdateUser)
	a.Router.Methods("PATCH").Path("/users/{userId:[0-9]+}/reset").HandlerFunc(controller.ResetPassword)
	a.Router.Methods("DELETE").Path("/users/{userId:[0-9]+}").HandlerFunc(controller.DeleteUser)
}
