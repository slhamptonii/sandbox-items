package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v3/model"
)

//endpoint to create a new item
func CreateItem(w http.ResponseWriter, r *http.Request) {
	newItem, err := payloadToItem(r)
	if err != nil {
		log.Println("error maping request to item", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	err = r.Body.Close()
	if err != nil {
		log.Println("error handling request", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	vars := mux.Vars(r)
	userId := vars["userId"]

	//convert userId parameter to an int
	id, err := strconv.Atoi(userId)
	if err != nil {
		log.Println("could not convert id to int", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	if id != newItem.Owner {
		log.Println("unauthorized request. user ids do not match", id, newItem.Owner)
		respondWithError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	newItem.CreatedDateTime = time.Now().UTC().Format(time.UnixDate)
	newItem.UpdatedDateTime = time.Now().UTC().Format(time.UnixDate)

	err = db.QueryRow(
		"INSERT INTO dnd.item(user_id, name, description, created_date_time, updated_date_time) VALUES($1, $2, $3, $4, $5) RETURNING id",
		newItem.Owner, newItem.Name, newItem.Description, newItem.CreatedDateTime, newItem.UpdatedDateTime).Scan(&newItem.Id)

	if err != nil {
		log.Println("could not create item", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	log.Println("created item", newItem.Id)

	respondWithJSON(w, http.StatusCreated, newItem)
}

func GetItems(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userId := vars["userId"]

	//convert userId parameter to an int
	id, err := strconv.Atoi(userId)
	if err != nil {
		log.Println("could not convert user id to int", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	//TODO: set in config
	countMin, countMax := 1, 100
	itemCount := 1
	c := r.URL.Query().Get("count")

	if c != "" {
		count, err := strconv.Atoi(c)
		if err != nil {
			log.Println("could not convert count to int", err.Error())
			respondWithError(w, http.StatusBadRequest, "invalid count")
			return
		}

		if count < countMin {
			log.Println("moving count from", count, "to", countMin)
			count = countMin
		} else if count > countMax {
			log.Println("moving count from", count, "to", countMax)
			count = countMax
		}

		itemCount = count
	}

	items := make([]model.Item, 0)
	rows, err := db.Query("SELECT * FROM dnd.item WHERE user_id = $1 LIMIT $2", id, itemCount)
	if err != nil {
		if err.Error() == "sql: no rows in result set" {
			respondWithJSON(w, http.StatusOK, items)
			return
		} else {
			respondWithError(w, http.StatusInternalServerError, "internal service error")
			return
		}
	}

	for rows.Next() {
		var newItem model.Item
		if err := rows.Scan(&newItem.Id, &newItem.Owner, &newItem.Name, &newItem.Description, &newItem.CreatedDateTime, &newItem.UpdatedDateTime); err != nil {
			log.Println("error reading row", err.Error())
			continue
		}

		items = append(items, newItem)
	}

	err = rows.Close()
	if err != nil {
		log.Println("error closing rows", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	respondWithJSON(w, http.StatusOK, items)
	return
}

func GetItem(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userId, itemId := vars["userId"], vars["itemId"]

	//convert userId parameter to an int
	_userId, err := strconv.Atoi(userId)
	if err != nil {
		log.Println("could not convert user id to int", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	_itemId, err := strconv.Atoi(itemId)
	if err != nil {
		log.Println("could not convert item id to int", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	item := model.Item{}
	err = db.QueryRow("SELECT * FROM dnd.item WHERE id = $1 AND user_id = $2", _itemId, _userId).Scan(
		&item.Id, &item.Owner, &item.Name, &item.Description, &item.CreatedDateTime, &item.UpdatedDateTime)
	if err != nil {
		log.Println("error querying item", err.Error())
		if err.Error() == "sql: no rows in result set" {
			respondWithError(w, http.StatusNotFound, "item not found")
			return
		} else {
			respondWithError(w, http.StatusInternalServerError, "internal service error")
			return
		}
	}

	respondWithJSON(w, http.StatusOK, item)
	return
}

// For updating the Name or Description of a User's Item
func UpdateItem(w http.ResponseWriter, r *http.Request) {
	item, err := payloadToItem(r)
	if err != nil {
		log.Println("error maping request to item", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	err = r.Body.Close()
	if err != nil {
		log.Println("error handling request", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	vars := mux.Vars(r)
	userId, itemId := vars["userId"], vars["itemId"]

	//convert userId parameter to an int
	_userId, err := strconv.Atoi(userId)
	if err != nil {
		log.Println("could not convert user id to int", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	_itemId, err := strconv.Atoi(itemId)
	if err != nil {
		log.Println("could not convert item id to int", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	if item.Name == "" || _itemId != item.Id {
		respondWithError(w, http.StatusBadRequest, "invalid changes")
		return
	}

	if _userId != item.Owner {
		log.Println("unauthorized request. user ids do not match", _userId, item.Owner)
		respondWithError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	_item := model.Item{}
	err = db.QueryRow("SELECT * FROM dnd.item WHERE id = $1 AND user_id = $2", _itemId, _userId).Scan(
		&_item.Id, &_item.Owner, &_item.Name, &_item.Description, &_item.CreatedDateTime, &_item.UpdatedDateTime)
	if err != nil {
		log.Println("error querying item", err.Error())
		if err.Error() == "sql: no rows in result set" {
			respondWithError(w, http.StatusNotFound, "item not found")
			return
		} else {
			respondWithError(w, http.StatusInternalServerError, "internal service error")
			return
		}
	}

	item.Id = _item.Id
	item.Owner = _item.Owner
	item.CreatedDateTime = _item.CreatedDateTime
	item.UpdatedDateTime = time.Now().UTC().Format(time.UnixDate)

	_, err = db.Exec("UPDATE dnd.item SET name = $1, description = $2, updated_date_time = $3 WHERE id = $4 AND user_id = $5",
		item.Name, item.Description, item.CreatedDateTime, item.Id, item.Owner)
	if err != nil {
		log.Println("error querying item", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	respondWithJSON(w, http.StatusOK, item)
	return
}

// For changing the Ownership of an Item from one User to another
func ResetItem(w http.ResponseWriter, r *http.Request) {
	item, err := payloadToItem(r)
	if err != nil {
		log.Println("error maping request to item", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	err = r.Body.Close()
	if err != nil {
		log.Println("error handling request", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	vars := mux.Vars(r)
	userId, itemId := vars["userId"], vars["itemId"]

	//convert userId parameter to an int
	_userId, err := strconv.Atoi(userId)
	if err != nil {
		log.Println("could not convert user id to int", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	_itemId, err := strconv.Atoi(itemId)
	if err != nil {
		log.Println("could not convert item id to int", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	if item.Name == "" || _itemId != item.Id {
		respondWithError(w, http.StatusBadRequest, "invalid changes")
		return
	}

	if _userId == item.Owner && 0 != item.Owner {
		log.Println("unauthorized request. user ids do not match", _userId, item.Owner)
		respondWithError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	item.CreatedDateTime = time.Now().UTC().Format(time.UnixDate)
	item.UpdatedDateTime = time.Now().UTC().Format(time.UnixDate)

	err = db.QueryRow(
		"INSERT INTO dnd.item(user_id, name, description, created_date_time, updated_date_time) VALUES($1, $2, $3, $4, $5) RETURNING id",
		item.Owner, item.Name, item.Description, item.CreatedDateTime, item.UpdatedDateTime).Scan(&item.Id)

	if err != nil {
		log.Println("could not create item", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	log.Println("reset item", item.Id)

	_, err = db.Exec("DELETE FROM dnd.item WHERE id = $1 AND user_id = $2", _itemId, _userId)
	if err != nil {
		log.Println("error querying item", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	log.Println("delted _item", _itemId)

	respondWithJSON(w, http.StatusOK, item)
	return
}

func DeleteItem(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userId, itemId := vars["userId"], vars["itemId"]

	//convert userId parameter to an int
	_userId, err := strconv.Atoi(userId)
	if err != nil {
		log.Println("could not convert user id to int", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	_itemId, err := strconv.Atoi(itemId)
	if err != nil {
		log.Println("could not convert item id to int", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	if _userId < 0 {
		log.Println("invalid user", _userId)
		respondWithError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	if _itemId < 0 {
		log.Println("invalid item", _itemId)
		respondWithError(w, http.StatusBadRequest, "bad request")
		return
	}

	_, err = db.Exec("DELETE FROM dnd.item WHERE id = $1 AND user_id = $2", _itemId, _userId)
	if err != nil {
		log.Println("error querying item", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	log.Println("delted _item", _itemId)

	respondWithJSON(w, http.StatusNoContent, nil)
	return
}

//Converts HTTP request body into a model.Item struct
func payloadToItem(r *http.Request) (model.Item, error) {
	var item model.Item
	err := json.NewDecoder(r.Body).Decode(&item)
	if err != nil {
		log.Println("error decoding item", err.Error())
	}
	return item, err
}

//Clears signed string token and sets HTTP Response body to map of error messages
func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

//Sets status, body, and default headers to HTTP Response
func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	_, _ = w.Write(response)
}
