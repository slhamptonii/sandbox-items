// Copyright 2021, Sheldon L. Hampton II, All rights reserved.
package main

import (
	"context"
	"database/sql"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"gopkg.in/yaml.v2"
)

var db *sql.DB

func main() {
	log.Println("Hello, World!")
	env := flag.String("env", "local", "environment where application will be running")
	port := flag.String("port", "80", "port exposed for outside traffic")
	flag.Parse()

	path := fmt.Sprintf("./env/%s.yml", *env)
	envPath, _ := filepath.Abs(path)

	data, err := ioutil.ReadFile(envPath)
	if err != nil {
		log.Fatalf("could not read env config file %v", err)
	}

	var config map[string]string
	err = yaml.Unmarshal(data, &config)
	if err != nil {
		log.Fatalf("could not unmarshall yaml file %v", err)
	}

	//connect to database
	connectionString := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		config["host"], config["port"], config["username"],
		config["password"], config["database"], config["ssl"])

	db, err = sql.Open("postgres", connectionString)
	if err != nil {
		log.Fatal("unable to connect to database", err.Error())
	}

	//test connection
	if err := db.Ping(); err != nil {
		log.Fatal("could not ping database ", err.Error())
	}

	log.Println("successfully connected to database")

	router := mux.NewRouter()
	router.Methods("HEAD").Path("/health").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println("healthy")
		w.WriteHeader(http.StatusOK)
	})

	router.Methods("POST").Path("/users/{userId:[0-9]+}/items").HandlerFunc(CreateItem)
	router.Methods("GET").Path("/users/{userId:[0-9]+}/items").HandlerFunc(GetItems)
	router.Methods("GET").Path("/users/{userId:[0-9]+}/items/{itemId:[0-9]+}").HandlerFunc(GetItem)
	router.Methods("PATCH").Path("/users/{userId:[0-9]+}/items/{itemId:[0-9]+}").HandlerFunc(UpdateItem)
	router.Methods("PUT").Path("/users/{userId:[0-9]+}/items/{itemId:[0-9]+}").HandlerFunc(ResetItem)
	router.Methods("DELETE").Path("/users/{userId:[0-9]+}/items/{itemId:[0-9]+}").HandlerFunc(DeleteItem)

	srv := &http.Server{
		Handler:      router,
		Addr:         ":" + *port,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	go func() {
		log.Println("Starting Server")
		if err := srv.ListenAndServe(); err != nil {
			log.Fatal("application closing ", err.Error())
		}
	}()

	// Graceful Shutdown
	waitForShutdown(srv)
}

func waitForShutdown(srv *http.Server) {
	interruptChan := make(chan os.Signal, 1)
	signal.Notify(interruptChan, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	// Block until we receive our signal.
	<-interruptChan

	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	srv.Shutdown(ctx)

	log.Println("Goodbye, cruel World!")
	os.Exit(0)
}
